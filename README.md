## Build

Run ` docker build -t backend-node . ` to build the project. 

```
docker build -t backend-node .
```


## Development server

Run ` docker-compose up ` for a dev server. Navigate to `http://localhost:3000/`. The app will automatically reload if you change any of the source files.

```
docker-compose up
```

to start in :  `http://localhost:3000/`


```
http://localhost:3000/
```
