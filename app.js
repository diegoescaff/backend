// Requires
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var CronJob = require('cron').CronJob;
var request = require('request');

// Inicializar variables
var app = express();

// CORS
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept'
    );
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    next();
});

// Body Parser
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Importar rutas
var appRoutes = require('./src/routes/app');
var pruebaRoutes = require('./src/routes/prueba');

// Conexión a la base de datos
mongoose.connection.openUri('mongodb://mongodb:27017/test', (err, res) => {
    if (err) throw err;
    console.log('Base de datos: \x1b[32m%s\x1b[0m', 'online');
});

// Rutas
app.use('/prueba', pruebaRoutes);
app.use('/', appRoutes);

var job = new CronJob(
    '59 * * * *',
    function() {
        request('http://mongodb:3000/', function(error, response, body) {
            if (!error && response.statusCode === 200) {
                console.log('Actualización de HITS');
            }
        });
    },
    null,
    true,
    'America/Los_Angeles'
);
job.start();

// Escuchar peticiones
app.listen(3000, () => {
    // app.listen(3000, '192.168.43.11', () => {
    console.log('Backend puerto 3000: \x1b[32m%s\x1b[0m', 'online');
});