var express = require('express');
var app = express();

var request = require('request');

var url = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';
var urlMongo = 'mongodb://mongodb:27017/test';
var MongoClient = require('mongodb').MongoClient;

app.get('/', async(req, res, next) => {
    console.log('get');
    const respuesta = obtenerDatos();
    respuesta
        .then(data => {
            data.hits.map(async e => {
                await insertAllData(e);
            });

            res.status(200).json({
                ok: true,
                mensaje: 'Peticion realizada correctamente'
            });
        })
        .catch(error => {
            res.status(400).json({
                ok: false,
                mensaje: 'Error al Realizar la Peticion',
                error: error
            });
        });
});

async function obtenerDatos() {
    return new Promise((resolve, reject) => {
        request(url, function(error, response, body) {
            console.error('error:', error); // Print the error if one occurred
            console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            // console.log('body:', body); // Print the HTML for the Google homepage.
            if (!error && response.statusCode === 200) {
                resolve(JSON.parse(body));
            } else {
                reject(error);
            }
        });
    });
}

async function insertAllData(hists) {
    MongoClient.connect(urlMongo, function(err, db) {
        if (err) {
            console.error(
                'Error al conectarse a mongo , Descripticion del Error: ',
                err
            );
        }
        hists.deleted_at = null;
        db.collection('hits').insertOne(hists, function(err, res) {
            if (err) {
                console.error('Error al Insertar , Descripticion del Error: ', err);
            }
            console.log('Document hits inserted del autor :', hists.objectID);
            // close the connection to db when you are done with it
            db.close();
        });
    });
}

module.exports = app;