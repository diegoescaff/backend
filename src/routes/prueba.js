var express = require('express');
var app = express();

var urlMongo = 'mongodb://mongodb:27017/test';
var MongoClient = require('mongodb').MongoClient;

// ==========================================
// Obtener todos los Hits
// ==========================================
app.get('/', async(req, res, next) => {
    await getAllData()
        .then(resultado => {
            res.status(200).json({
                ok: true,
                mensaje: 'Peticion realizada correctamente',
                hits: resultado
            });
        })
        .catch(err => {
            res.status(400).json({
                ok: false,
                mensaje: 'Error al Realizar la Peticion',
                error: err
            });
        });
});

// ==========================================
// Eliminar de forma logica un Hits
// ==========================================
app.put('/:id', async(req, res) => {
    let _id = req.params.id;
    await deletedHits(_id)
        .then(resultado => {
            res.status(200).json({
                ok: true,
                mensaje: 'Peticion realizada correctamente',
                hits: resultado
            });
        })
        .catch(err => {
            res.status(400).json({
                ok: false,
                mensaje: 'Error al Realizar la Peticion',
                error: err
            });
        });
});
async function getAllData() {
    return new Promise((resolve, reject) => {
        MongoClient.connect(urlMongo, function(err, db) {
            if (err) {
                console.error(
                    'Error al conectarse a mongo , Descripticion del Error: ',
                    err
                );
                reject(err);
            }
            db.collection('hits')
                .find({ deleted_at: null })
                .sort({ created_at: -1 })
                .toArray(function(err, res) {
                    if (err) {
                        console.error('Error al Insertar , Descripticion del Error: ', err);
                        reject(err);
                    }
                    // close the connection to db when you are done with it
                    resolve(res);
                    db.close();
                });
        });
    });
}

async function deletedHits(hists) {
    return new Promise((resolve, reject) => {
        MongoClient.connect(urlMongo, function(err, db) {
            if (err) {
                console.error(
                    'Error al conectarse a mongo , Descripticion del Error: ',
                    err
                );
                reject(err);
            }
            let ObjectId = require('mongodb').ObjectID;

            db.collection('hits').update({ _id: ObjectId(hists) }, { $set: { deleted_at: 'asdasdasd' } },
                function(err, res) {
                    if (err) {
                        console.error('Error al Update , Descripticion del Error: ', err);
                        reject(err);
                    }
                    console.log('Document hits Update del autor :', res.result.n);
                    // close the connection to db when you are done with it
                    resolve(res);
                    db.close();
                }
            );
        });
    });
}

module.exports = app;